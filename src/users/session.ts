const uuidv4 = require('uuid/v4');

export class Session {
    /** Login Token **/
    token: string;

    constructor(username: string, password: string) {
        this.token = uuidv4();
    }

    /**
     * Set Token
     * @param token
     */
    getToken(): string {
        return this.token;
    }
}