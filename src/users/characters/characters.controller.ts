import {Controller, Get, Header} from '@nestjs/common';

@Controller('char')
export class CharactersController {

    @Get('list')
    @Header('Content-Type','application/json')
    listCharactors(): string {
        return '{"Testing":"test"}';
    }
}
