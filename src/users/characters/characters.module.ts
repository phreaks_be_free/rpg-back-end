import {MiddlewareConsumer, Module, NestModule} from '@nestjs/common';
import {CharactersController} from './characters.controller';
import {LoginMiddleware} from "../../middleware/login";

@Module({
    controllers: [CharactersController]
})
export class CharactersModule implements NestModule {
    configure(userAgent: MiddlewareConsumer) {
        /** Apply Login Middleware to End-points **/
        userAgent.apply(LoginMiddleware).forRoutes(CharactersController);
    }
}
