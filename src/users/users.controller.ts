import {Body, Controller, Get, Param, Post} from '@nestjs/common';
import {Session} from "./session";

@Controller('users')
export class UsersController {
    @Post('login')
    login(@Body('username') username, @Body('password') password) {
        let session = new Session(username, password);
        return session.getToken();
    }
}
