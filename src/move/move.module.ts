import { Module } from '@nestjs/common';
import { MoveController } from './move.controller';

@Module({
  controllers: [MoveController]
})
export class MoveModule {}
