import {HttpException, HttpStatus} from "@nestjs/common";

export class UnauthorizedException extends HttpException {
    constructor() {
        //todo: Add Logger Here
        super('Unauthorized Access', HttpStatus.UNAUTHORIZED)
    }
}