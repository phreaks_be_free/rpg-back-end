import { Module, NestModule} from '@nestjs/common';
import { CharactersModule } from './users/characters/characters.module';
import { UsersModule } from './users/users.module';
import { MoveModule } from './move/move.module';


@Module({
    imports: [CharactersModule, UsersModule, MoveModule],
    controllers: [],
    providers: [],
})
export class AppModule{
}
